package com.opalis.model.module;

import com.opalis.presenter.OnDataQueryComplete;
import com.opalis.model.data.UserDataHolder;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DataQueryModule {

    private Statement user_query_statement;
    private OnDataQueryComplete onDataQueryComplete;

    public DataQueryModule(OnDataQueryComplete onDataQueryComplete){
        try {
            this.onDataQueryComplete = onDataQueryComplete;

            if (UserDataHolder.getInstance() == null) {
                UserDataHolder.setInstance();
            }

            setupQueryStatement();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setupQueryStatement(){
        try {
            Class.forName("org.postgresql.Driver");
        }
        catch (java.lang.ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }

        String url = "jdbc:postgresql://salt.db.elephantsql.com:5432/bupwaice";
        String username = "bupwaice";
        String password = "weBEHynVXJBu0N0MmcKb8wAPeFhZd2zN";

        try {
            Connection db = DriverManager.getConnection(url, username, password);
            user_query_statement = db.createStatement();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void fetchUserList(String userMail){
        try {
            ResultSet queryUserDetails = user_query_statement.executeQuery("SELECT * FROM user_list WHERE user_mail='"+userMail+"'");
            if(queryUserDetails.next()) {
                UserDataHolder.getInstance().addUserData(
                        queryUserDetails.getString(1), queryUserDetails.getString(2),
                        queryUserDetails.getString(4), queryUserDetails.getString(5),
                        queryUserDetails.getString(6), queryUserDetails.getString(7),
                        queryUserDetails.getString(8));
            }

            queryUserDetails.close();
            user_query_statement.close();

            onDataQueryComplete.showQueryResults();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void checkUserAuthentication(String userMail, String userPassword) {
        try {
            ResultSet userCredentialQuery = user_query_statement.executeQuery("SELECT user_pass FROM user_list WHERE user_mail='" + userMail + "'");
            if(userCredentialQuery.next()){
                String userPassHash = userCredentialQuery.getString(1);
                if(userPassHash.equals(userPassword)){
                    fetchUserList(userMail);
                }else {
                    onDataQueryComplete.showQueryError("Wrong password");
                }
            }else {
                onDataQueryComplete.showQueryError("No such user");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void uploadBookToList(String bookIsbn){
        try {
            String userMail = UserDataHolder.getInstance().getUserMail();
            if(!userMail.equals("")) {
                user_query_statement.executeUpdate("UPDATE user_list SET user_book_isbn = array_cat(user_book_isbn, '{" + bookIsbn
                        + "}'), user_book_stat= array_cat(user_book_stat, '{n}'), user_book_part = array_cat(user_book_part, '{n}') WHERE user_mail = '" + userMail + "'");

                onDataQueryComplete.showQueryResults();
            }else {
                onDataQueryComplete.showQueryError("User not signed");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
