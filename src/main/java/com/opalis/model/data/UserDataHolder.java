package com.opalis.model.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserDataHolder {

    private static UserDataHolder UserDataHolderInstance = null;

    private static String user_mail;
    private static String user_name;
    private static String user_addr;
    private static String user_phon;
    private static List<String> user_book_isbn;
    private static List<String> user_book_stat;
    private static List<String> user_book_part;

    private UserDataHolder(){
        user_mail = user_name = user_phon = user_addr = "";
        user_book_isbn = new ArrayList<>();
        user_book_stat = new ArrayList<>();
        user_book_part = new ArrayList<>();
    }

    public static void setInstance(){
        UserDataHolderInstance = new UserDataHolder();
    }

    public static UserDataHolder getInstance(){
        return UserDataHolderInstance;
    }

    public void addUserData(String mail, String name,
                            String addr, String phon, String book_isbn,
                            String book_stat, String book_part){
        user_mail = mail;
        user_name = name;
        user_addr = addr;
        user_phon = phon;

        book_isbn = book_isbn.substring(1,book_isbn.length()-1);
        book_stat = book_stat.substring(1,book_stat.length()-1);
        book_part = book_part.substring(1,book_part.length()-1);

        user_book_isbn = new ArrayList<String>(Arrays.asList(book_isbn.split(",")));
        user_book_stat = new ArrayList<String>(Arrays.asList(book_stat.split(",")));
        user_book_part = new ArrayList<String>(Arrays.asList(book_part.split(",")));
    }

    public String getUserMail(){
        return user_mail;
    }

    public String getUserName(){
        return user_name;
    }

    public String getUserAddr(){
        return user_addr;
    }

    public String getUserPhon(){
        return user_phon;
    }

    public String getUserBookIsbn(int position){
        return user_book_isbn.get(position);
    }

    public String getUserBookStat(int position){
        return user_book_stat.get(position);
    }

    public String getUserBookPart(int position){
        return user_book_part.get(position);
    }

    public int getUserBookCount(){
        return user_book_isbn.size();
    }
}
