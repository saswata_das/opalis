package com.opalis.presenter;

public interface OnDataQueryComplete {
    public void showQueryResults();
    public void showQueryError(String queryErrorMessage);
}
