package com.opalis.view;

import com.opalis.model.data.UserDataHolder;
import com.opalis.model.module.DataQueryModule;
import com.opalis.presenter.OnDataQueryComplete;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.HashMap;

@RestController
@EnableAutoConfiguration
public class Main {

    public static void main(String[] args) {

        try {
            System.out.println("\n  --     --      --   |    -----  ----");
            System.out.println("/    \\  |   \\   /  \\  |      |   |    ");
            System.out.println("|    |  |   |  | -- | |      |    --- ");
            System.out.println("\\    /  |--    |    | |  |   |       |");
            System.out.println("  --    |      |    |  --  ----- ---- ");
            System.out.println("  **    *****   *****  **  ***** ****");

            SpringApplication.run(Main.class, args);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/signInUser", headers = "Accept=application/json", method = RequestMethod.POST)
    DeferredResult<HashMap<String,String>> signInUser(@RequestBody HashMap<String, String> userCredentials){
        DeferredResult<HashMap<String,String>> signQueryResult = new DeferredResult<>();
        try{
            DataQueryModule dataQueryModule = new DataQueryModule(new OnDataQueryComplete() {
                @Override
                public void showQueryResults() {
                    UserDataHolder userDataHolder = UserDataHolder.getInstance();
                    HashMap<String,String> userDetailsResult = new HashMap<>();

                    userDetailsResult.put("user_mail",userDataHolder.getUserMail());
                    userDetailsResult.put("user_name",userDataHolder.getUserName());
                    userDetailsResult.put("user_addr",userDataHolder.getUserAddr());
                    userDetailsResult.put("user_phon",userDataHolder.getUserPhon());

                    StringBuilder bookList = new StringBuilder();
                    for (int i = 0; i < userDataHolder.getUserBookCount(); i++) {
                        bookList.append(userDataHolder.getUserBookIsbn(i)).append(" - ").append(userDataHolder.getUserBookStat(i))
                                .append(" - ").append(userDataHolder.getUserBookPart(i)).append(" | ");
                    }
                    userDetailsResult.put("book_list",bookList.toString());

                    signQueryResult.setResult(userDetailsResult);
                }

                @Override
                public void showQueryError(String queryErrorMessage) {
                    HashMap<String, String> errorMessage = new HashMap<>();
                    errorMessage.put("err_msg",queryErrorMessage);
                    signQueryResult.setResult(errorMessage);
                }
            });
            dataQueryModule.checkUserAuthentication(userCredentials.get("user_mail"), userCredentials.get("user_pass"));
        }catch (Exception e){
            e.printStackTrace();
        }
        return signQueryResult;
    }

    @RequestMapping(value = "/addBookToList", headers = "Accept=application/json", method = RequestMethod.POST)
    DeferredResult<HashMap<String,String>> addBookToList(@RequestBody HashMap<String,String> newBookDetails){
        DeferredResult<HashMap<String,String>> bookAddQueryResult = new DeferredResult<>();
        try {
            DataQueryModule dataQueryModule = new DataQueryModule(new OnDataQueryComplete() {
                @Override
                public void showQueryResults() {
                    HashMap<String,String> userDetailsResult = new HashMap<>();
                    userDetailsResult.put("query_msg","Book Uploaded");
                    bookAddQueryResult.setResult(userDetailsResult);
                }

                @Override
                public void showQueryError(String queryErrorMessage) {
                    HashMap<String, String> errorMessage = new HashMap<>();
                    errorMessage.put("err_msg",queryErrorMessage);
                    bookAddQueryResult.setResult(errorMessage);
                }
            });
            dataQueryModule.uploadBookToList(newBookDetails.get("book_isbn"));
        }catch (Exception e){
            e.printStackTrace();
        }
        return bookAddQueryResult;
    }
}